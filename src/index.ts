#!/usr/bin/env node
import yargs, { Arguments } from "yargs";
import { hideBin } from "yargs/helpers";
import e2js from "./e2js";

yargs(hideBin(process.argv))
    .command('e2js [input] [output]', 'start the server', (yargs) => {
        return yargs
            .positional('input', {
                describe: 'path to input',
            })
            .positional('output', {
                describe: 'path to output',
            })
    }, (argv: Arguments) => {
        e2js(argv)
    })
    // .option('verbose', {
    //     alias: 'v',
    //     type: 'boolean',
    //     description: 'Run with verbose logging'
    // })
    .parse()