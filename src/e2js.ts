/// <reference path="types.d.ts" />

import { writeJsonSync } from "fs-extra";
import { parse } from "node-xlsx";
import { resolve } from 'path';
import { Arguments } from "yargs";
export default async (args: Arguments) => {
    const input = args.input as string;
    const output = (args.output || __dirname) as string;
    if (!input && typeof input !== 'string') {
        console.log('missing input path for xlsx');
        process.exit(1);
    }
    const workSheetsFromFile = parse(resolve(input));
    const result: MapItem[] = workSheetsFromFile.map(({ name, data }): MapItem => ({
        [name]: data.reduce((pre, [key, value]): MapItem => ({
            ...pre, [key as string]: value
        }), {})
    }
    ))
    writeJsonSync(resolve(output, 'result.json'), result, { encoding: 'utf-8' })

}