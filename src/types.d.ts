type MapProperty = string;
type MapObject = {
    [key: string]: MapProperty | MapObject;
};
type MapItem = MapObject;